import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import * as FB from 'expo-facebook';
import { Platform, StatusBar, StyleSheet, View } from 'react-native';
import * as firebase from 'firebase';
import "firebase/database";

import useCachedResources from '../hooks/useCachedResources';
import BottomTabNavigator from '../navigation/BottomTabNavigator';
import LinkingConfiguration from '../navigation/LinkingConfiguration';

const Stack = createStackNavigator();
const validateFB = () => {
  FB.initializeAsync("598092020859676");
}

export default function App(props) {
  const isLoadingComplete = useCachedResources();
  validateFB();

  // Initialize Firebase
  const firebaseConfig = {
    apiKey: "AIzaSyCHMRa9iVAtMjhEwpTd",
    authDomain: "whatareyouchild.firebaseapp.com",
    projectId: "whatareyouchild",
    databaseURL: "https://whatareyouchild-3aabf.firebaseio.com",
    storageBucket: "whatareyouchild.appspot.com",
    messagingSenderId: "sender-id",
    appId: "1:974167858275:android:be43ff2c948fbdf8922c56",
    measurementId: "G-measurement-id"
  };

  if(firebase.apps.length == 0) {
    firebase.initializeApp(firebaseConfig);
  }

  var db = firebase.database();

  if (!isLoadingComplete) {
    return null;
  } else {
    return (
      <View style={styles.container}>
        {Platform.OS === 'ios' && <StatusBar barStyle="dark-content" />}
        <NavigationContainer linking={LinkingConfiguration}>
          <Stack.Navigator>
            <Stack.Screen name="PIN" component={BottomTabNavigator} />
          </Stack.Navigator>
        </NavigationContainer>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});